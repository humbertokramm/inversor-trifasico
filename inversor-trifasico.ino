/*#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif
BluetoothSerial SerialBT;*/

#include <Arduino.h>        //Não é necessário caso use Arduino IDE
#include "driver/mcpwm.h"   //inclui a biblioteca "Motor Control PWM" nativa do ESP32
#include  <Wire.h> // Necessário apenas para o Arduino 1.6.5 e posterior


// the number of the FASES pin

#define GPIO_PWM0A_OUT 21
#define GPIO_PWM0B_OUT 19

#define GPIO_PWM1A_OUT 17
#define GPIO_PWM1B_OUT 26

#define GPIO_PWM2A_OUT 16
#define GPIO_PWM2B_OUT 18

// Definições

// Mínima frequência de operação.
#define FREQ_MIN (unsigned int)10
// Máxima frequência.
#define FREQ_MAX (unsigned int)100
// Faixa ADC
#define RANGE_ADC (unsigned int)4096

// Frequência da portadora.
#define FREQ_CARRIER 10000

// Tempos do passo do timer.
#define STEP_TIME ( 1000000 / FREQ_CARRIER )

// Tempo do incremento da frequencia.
#define STEP_RAMP_TIME 2000// n * 100us

// Duas vezes PI
#define TWO_PI ((float)6.283185307)

enum states{
	STOP,
	FAULT,
	RUN,
	RAMP_UP,
	RAMP_DOWN
} state;

//Led para inspeção
int ledState = LOW; 
const int ledPin = 2;
const int startPin = 5;
const int stopPin = 23;
const int faultPin = 22;


int sensorPin = A0;    // select the input pin for the potentiometer
int setPointFreq = FREQ_MIN;
int currentFreq = FREQ_MIN;
int lastCurrentFreq = FREQ_MIN;

const int resolution = 8;
const float pi_2 = 6.283185307;
const int maxStep = 2500;
float duty[maxStep];
int nStep = 0;
int defasagem = 0;

int stepR;
int stepS;
int stepT;



int freqPWM = 10000;
int timeStep = 1000000/freqPWM; // in us

//Interrupção
volatile int interruptCounter;

// Timer
hw_timer_t * timer = NULL;

// Modulação
unsigned int numberPulsePeriod;
unsigned int countPulsePeriod;

// Divisor passo de frequência
int countStepRamp;

// Sinal para passo.
bool flagStep;

// Sinal motor energizado.
bool flagTurnOnMotor;

void
mcpwm_setup(void)
{
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, GPIO_PWM0A_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0B, GPIO_PWM0B_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM1A, GPIO_PWM1A_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM1B, GPIO_PWM1B_OUT); 
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM2A, GPIO_PWM2A_OUT);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM2B, GPIO_PWM2B_OUT); 
 
	mcpwm_config_t pwm_config;
 
	pwm_config.frequency = FREQ_CARRIER;                          //frequência
	pwm_config.cmpr_a = 0;                                //Ciclo de trabalho (duty cycle) do PWMxA = 0
	pwm_config.cmpr_b = 0;                                //Ciclo de trabalho (duty cycle) do PWMxb = 0
	pwm_config.counter_mode = MCPWM_UP_COUNTER;           //Para MCPWM assimetrico
	pwm_config.duty_mode = MCPWM_DUTY_MODE_0;             //Define ciclo de trabalho em nível alto
 
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config); //Define PWM0A & PWM0B com as configurações acima
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &pwm_config);
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_2, &pwm_config);
	
}

//Função que para o MCPWM de ambos os Operadores
void 
mcpwm_set_low_all(void)
{
	mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A); //Desliga o sinal do MCPWM no Operador A
	mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B); //Desliga o sinal do MCPWM no Operador B
	mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A); 
	mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_B); 
	mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_OPR_A); 
	mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_OPR_B); 
}

void
mcpwm_set(mcpwm_timer_t timer_num, float duty_cycle)
{
	if( duty_cycle < 2 )
	{
		duty_cycle = 2;
	}
	mcpwm_set_duty(MCPWM_UNIT_0, timer_num, MCPWM_OPR_A, duty_cycle - 1);              //Configura a porcentagem do PWM no Operador B (Ciclo de trabalho)
	mcpwm_set_duty(MCPWM_UNIT_0, timer_num, MCPWM_OPR_B, duty_cycle);              //Configura a porcentagem do PWM no Operador B (Ciclo de trabalho)
}

void IRAM_ATTR handleInterrupt()
{
	state = FAULT;
	TurnOffMotor();
}

void IRAM_ATTR onTimer() 
{
	float rad;
	float pwmU, pwmV, pwmW;
	
	// Divisor para tempo da rampa.
	if( countStepRamp < STEP_RAMP_TIME )
	{
		countStepRamp ++;
	}
	else
	{
		flagStep = true;
		countStepRamp = 0;
	}
	
	// Modulando.
	if( flagTurnOnMotor == true )
	{
		// Nova frequência apenas no fim do ciclo da última.
		if( countPulsePeriod == 0 )
		{
			if( lastCurrentFreq != currentFreq )
			{
				lastCurrentFreq = currentFreq;
				// Novo número de pulsos.
				numberPulsePeriod = FREQ_CARRIER / currentFreq;
			}
		}
		
		rad = ( (float)countPulsePeriod / (float)numberPulsePeriod ) * TWO_PI;
		
		pwmU = 49 * ( sin( rad ) + 1 );
 		pwmV = 49 * ( sin( rad + ( TWO_PI / 3 ) ) + 1 );
		pwmW = 49 * ( sin( rad + ( TWO_PI * (float)2/3) ) + 1 );
			
		mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);
		mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_1);
		mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_2);
		
		mcpwm_set(MCPWM_TIMER_0, pwmU);
		mcpwm_set(MCPWM_TIMER_1, pwmV);
		mcpwm_set(MCPWM_TIMER_2, pwmW);
		mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_0);
		mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_1);
		mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_2);
		
		// Contador do periodo.
		if( countPulsePeriod < numberPulsePeriod )
		{
			countPulsePeriod ++;
		}else
		{
			countPulsePeriod = 0;
		}
	}
}

void
TurnOffMotor(void)
{
	flagTurnOnMotor = false;
	mcpwm_set_low_all();
}

void
TurnOnMotor(void)
{
	lastCurrentFreq = -1;
	countPulsePeriod = 0;
	
	mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, MCPWM_DUTY_MODE_0); 
	mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_B, MCPWM_DUTY_MODE_1);
	
	mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A, MCPWM_DUTY_MODE_0); 
	mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_B, MCPWM_DUTY_MODE_1);
	
	mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_OPR_A, MCPWM_DUTY_MODE_0); 
	mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_OPR_B, MCPWM_DUTY_MODE_1);
	
	mcpwm_deadtime_enable(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_BYPASS_FED, 10, 10);   //Deadtime of 1us
	mcpwm_deadtime_enable(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_BYPASS_FED, 10, 10);
	mcpwm_deadtime_enable(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_BYPASS_FED, 10, 10);
	
	flagTurnOnMotor = true;
}

bool
CheckStop(void)
{
	if( digitalRead(stopPin) == LOW )
	{
		
		return true;
	}
	else
	{
		return false; 
	}
}

bool
CheckStart(void)
{
	if( digitalRead(startPin) == LOW )
	{
		return true;
	}
	else
	{
		return false; 
	}
}

bool
CheckFault(void)
{
	if( digitalRead(faultPin) == LOW )
	{
		return true;
	}
	else
	{
		return false; 
	}
}

int
GetFreq(void)
{
	static int retFreq;
	
	if( CheckStart() == true )
	{
		retFreq = ( analogRead(sensorPin) / ( RANGE_ADC / ( FREQ_MAX - FREQ_MIN )) + FREQ_MIN );
	}
	return retFreq;
}

void checkState()
{
	
	int freq;
	
	switch(state){
		
		case FAULT:
		// Mantem em falha quando entrada ativa.
		if( CheckFault() == true )
		{
			break;
		}
		case STOP:
		// Espera comando para rodar.
		if( CheckStart() == true )
		{
			currentFreq = FREQ_MIN;
			state = RUN;
			TurnOnMotor();
		}
		break;
		
		case RUN:
		// Verifica solicitação de parada.
		if( CheckStop() == true )
		{
			TurnOffMotor();
			state = STOP;
			break;
		}
		// Espera por novo valor frequência válido.
		// Limites de Frequência de operação.
		freq = GetFreq();
		if( freq > FREQ_MIN || freq < FREQ_MAX )
		{
			// Novo valor de frequência?
			if( freq > currentFreq )
			{
				state = RAMP_UP;
				setPointFreq = freq;
			}
			else if( freq < currentFreq )
			{
				state = RAMP_DOWN;
				setPointFreq = freq;
			}
		}
		break;
		
		// Transição entre frequências.
		case RAMP_UP:
		if( flagStep  == true )
		{
			flagStep = false;
			if( currentFreq < setPointFreq )
			{
				currentFreq ++;
			}
			else
			{
				state = RUN;
			}
		}
		break;
		
		case RAMP_DOWN:
		if( flagStep  == true )
		{
			flagStep = false;
			if( currentFreq > setPointFreq )
			{
				currentFreq --;
			}
			else
			{
				state = RUN;
			}
		}
		break;
	}
}

void setup(){
	// configure LED PWM functionalitites
	pinMode(ledPin, OUTPUT);
	digitalWrite(ledPin, HIGH);
	pinMode(startPin, INPUT);
	pinMode(stopPin, INPUT);

	// Interrupção externa, indicação de falha.
	pinMode(faultPin, INPUT);
	attachInterrupt(digitalPinToInterrupt(faultPin), handleInterrupt, FALLING);

	//CMPWM
	mcpwm_setup();
	mcpwm_set_low_all();

	//Timer
	timer = timerBegin(0, 80, true);
	timerAttachInterrupt(timer, &onTimer, true);
	timerAlarmWrite(timer, STEP_TIME, true);
	timerAlarmEnable(timer);

	// Estado inicial para maquina de estados.
	state = STOP;
}

void loop()
{
	// Máquina de estados inversor frequência.
	checkState();
}







