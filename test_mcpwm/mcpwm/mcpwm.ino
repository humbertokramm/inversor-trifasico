

#include <Arduino.h>        //Não é necessário caso use Arduino IDE
#include "driver/mcpwm.h"   //inclui a biblioteca "Motor Control PWM" nativa do ESP32
#include  <Wire.h> // Necessário apenas para o Arduino 1.6.5 e posterior

  
#define GPIO_PWM0A_OUT 2   //Declara GPIO 12 como PWM0A
#define GPIO_PWM0B_OUT 27   //Declara GPIO 14 como PWM0B



//Função que configura o MCPWM operador A (Unidade, Timer, Porcentagem (ciclo de trabalho))
static void brushed_motor_forward(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle)
{
  //mcpwm_set_signal_low(unidade PWM(0 ou 1), Número do timer(0, 1 ou 2), Operador (A ou B));    => Desliga o sinal do MCPWM no Operador B (Define o sinal em Baixo)
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B);
 
  //mcpwm_set_duty(unidade PWM(0 ou 1), Número do timer(0, 1 ou 2), Operador (A ou B), Ciclo de trabalho (% do PWM));    => Configura a porcentagem do PWM no Operador A (Ciclo de trabalho)
    mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_A, duty_cycle);
         
  //mcpwm_set_duty_tyoe(unidade PWM(0 ou 1), Número do timer(0, 1 ou 2), Operador (A ou B), Nível do ciclo de trabalho (alto ou baixo));    => define o nível do ciclo de trabalho (alto ou baixo)        
    mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_A, MCPWM_DUTY_MODE_0);
    //Nota: Chame essa função toda vez que for chamado "mcpwm_set_signal_low" ou "mcpwm_set_signal_high" para manter o ciclo de trabalho configurado anteriormente

}
 
//Função que configura o MCPWM Do operador B (Unidade, Timer, Porcentagem (ciclo de trabalho))
static void brushed_motor_backward(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle)
{
    mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_A, duty_cycle-1);              //Configura a porcentagem do PWM no Operador B (Ciclo de trabalho)
    mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_A, MCPWM_DUTY_MODE_0);  //define o nível do ciclo de trabalho (alto ou baixo)
    
    mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_B, duty_cycle);              //Configura a porcentagem do PWM no Operador B (Ciclo de trabalho)
    mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_B, MCPWM_DUTY_MODE_1);  //define o nível do ciclo de trabalho (alto ou baixo)

    mcpwm_deadtime_enable(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_BYPASS_FED, 10, 10);   //Deadtime of 10us
}
 
//Função que para o MCPWM de ambos os Operadores
static void brushed_motor_stop(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num)
{
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_A); //Desliga o sinal do MCPWM no Operador A
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B); //Desliga o sinal do MCPWM no Operador B
}

void setup() {
  Serial.begin(115200);
 
  //mcpwm_gpio_init(unidade PWM 0, saida A, porta GPIO)     => Instancia o MCPWM0A no pino GPIO_PWM0A_OUT declarado no começo do código
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, GPIO_PWM0A_OUT);
 
  //mcpwm_gpio_init(unidade PWM 0, saida B, porta GPIO)     => Instancia o MCPWM0B no pino GPIO_PWM0B_OUT declarado no começo do código
  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0B, GPIO_PWM0B_OUT); 
 
  mcpwm_config_t pwm_config;
 
  pwm_config.frequency = 10000;                          //frequência = 500Hz,
  pwm_config.cmpr_a = 0;                                //Ciclo de trabalho (duty cycle) do PWMxA = 0
  pwm_config.cmpr_b = 0;                                //Ciclo de trabalho (duty cycle) do PWMxb = 0
  pwm_config.counter_mode = MCPWM_UP_COUNTER;           //Para MCPWM assimetrico
  pwm_config.duty_mode = MCPWM_DUTY_MODE_0;             //Define ciclo de trabalho em nível alto
  //Inicia(Unidade 0, Timer 0, Config PWM)
  mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config); //Define PWM0A & PWM0B com as configurações acima
}

void loop() {
  //Move o motor no sentido horário
  brushed_motor_backward(MCPWM_UNIT_0, MCPWM_TIMER_0, 50.0);
  delay(2000);
 
  //Para o motor
  brushed_motor_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);
  delay(2000);
 
  //Move o motor no sentido antihorário
  brushed_motor_backward(MCPWM_UNIT_0, MCPWM_TIMER_0, 25.0);
  delay(2000);
 
  //Para o motor
  brushed_motor_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);
  delay(2000);
 
  // Aceleracao i de 1 a 100
  for(int i=10;i<=100;i++){
    brushed_motor_backward(MCPWM_UNIT_0, MCPWM_TIMER_0, i);
    delay(200);
  }
 
  // Desaceleração i de 100 a 1
  delay(5000);
  for(int i=100;i>=10;i--){
    brushed_motor_backward(MCPWM_UNIT_0, MCPWM_TIMER_0, i);
    delay(100);
  }
  delay(5000);
 
}